package com.amdocs;
import static org.junit.Assert.*;
import org.junit.Test;

public class CalculatorTest {
    @Test
    public void testAdd() throws Exception {

        int k= new Calculator().add();
        assertEquals("Add", 9, k);

    }
    @Test
    public void testSub() throws Exception {

        int k= new Calculator().sub();
        assertEquals("Sub", 3, k);

    }
    @Test
    public void testDecreasecounter() throws Exception {

        int k= new Increment().decreasecounter(0);
        assertEquals("Firsttest", 0, k);
    }
    @Test
    public void testDecreasecounter2() throws Exception {
    	int t= new Increment().decreasecounter(1);
        assertEquals("Secondtest", 1, t);
    }
    @Test
    public void testDecreasecounter3() throws Exception {
        int w= new Increment().decreasecounter(3);
        assertEquals("Thirdtest", 2, w);
    }

}

